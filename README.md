# Unicode code generator

This is an R6RS Scheme program that converts parts of the Unicode data
to a Scheme program. While not a complete implementation of Unicode
for R6RS, it does provide 
`char-alphabetic?`,
`char-numeric?`,
`char-whitespace?`,
`char-upper-case?`,
`char-lower-case?`,
`char-title-case?`,
`char-general-category`,
`char-upcase`,
`char-downcase`,
`char-titlecase`,
and the extra procedure `char-width` that indicates how characters
behave on a character-cell based screen.

Please report any bugs you find.

For an alternative implementation, which is less likely to have
egregious bugs, see the one by Ghuloum and Dybvig in Chez Scheme.
