#!/usr/bin/env scheme-script
;; Copyright © 2013, 2017, 2018, 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Lovecraftian code to generate code for R6RS's char procedures.

;; The data/ directory should have the Unicode files.

(import
  (rnrs (6))
  (industria strings)
  (only (srfi :1 lists) iota append-map)
  (only (srfi :13 strings) string-suffix? string-contains string-filter)
  (prefix (srfi :14 char-sets) srfi14:)
  (only (srfi :27 random-bits) random-integer)
  (only (srfi :43 vectors) vector-swap! vector-copy vector-copy!)
  (srfi :48)
  (xitomatl AS-match))

(define Q 8)                         ;64-bit. Just for size estimates.

(define effort:general-category 100)
(define effort:best-widths 100)
(define effort:case-programs 1000)
;; (define effort:general-category 10000)
;; (define effort:best-widths 1000)
;; (define effort:case-programs 10000)

(when (member "--quick" (command-line))
  (set! effort:general-category 10)
  (set! effort:best-widths 10)
  (set! effort:case-programs 10))

(define (print . x)
  (for-each (lambda (x) (display x (current-error-port))) x)
  (newline (current-error-port)))

(define (pretty-print x)
  (format #t "~y" x))

(define (**print-copyright**)
  (display ";; -*- mode: scheme; coding: utf-8 -*-\n\
;; Copyright © 2013, 2017, 2019 Göran Weinholt <goran@weinholt.se>\n\
;; SPDX-License-Identifier: MIT\n\
#!r6rs\n\
\n"))

;; Inversion lists based on strings

(define (char-set-contains? cs c)
  (let ((ci (char->integer c)))
    (let lp ((low 0)
             (high (string-length cs)))
      (if (fx<? low high)
          (let ((middle (fxdiv (fx+ low high) 2)))
            (if (fx>=? ci (char->integer (string-ref cs middle)))
                (lp (fx+ middle 1) high)
                (lp low middle)))
          (fxodd? high)))))

(define (char-set-complement cs)
  (call-with-string-output-port
    (lambda (p)
      (cond ((or (fxzero? (string-length cs))
                 (not (eqv? (string-ref cs 0) #\nul)))
             (put-char p #\nul)
             (put-string p cs))
            (else
             (put-string p cs 1 (fx- (string-length cs) 1)))))))

(define (char-set-union2 cs1 cs2)
  (define N 0)
  (call-with-string-output-port
    (lambda (p)
      (let ((len1 (string-length cs1))
            (len2 (string-length cs2)))
        (let lp ((i1 0) (i2 0) (count 0))
          (cond ((and (fx<? i1 len1) (fx<? i2 len2))
                 (let ((c1 (string-ref cs1 i1))
                       (c2 (string-ref cs2 i2)))
                   (let-values (((i c i1 i2)
                                 (if (or (char<? c1 c2)
                                         (and (char=? c1 c2)
                                              (fxeven? i1)))
                                     (values i1 c1 (fx+ i1 1) i2)
                                     (values i2 c2 i1 (fx+ i2 1)))))
                     (cond ((fxeven? i)
                            (when (fx=? count N)
                              (put-char p c))
                            (lp i1 i2 (fx+ count 1)))
                           (else
                            (let ((count (fx- count 1)))
                              (when (fx=? count N)
                                (put-char p c))
                              (lp i1 i2 count)))))))
                (else
                 (let ((count (if (or (and (fx<? i1 len1) (fxodd? i1))
                                      (and (fx<? i2 len2) (fxodd? i2)))
                                  (fx- count 1)
                                  count)))
                   (when (fx=? count N)
                     (put-string p cs1 i1 (fx- len1 i1))
                     (put-string p cs2 i2 (fx- len2 i2)))))))))))

(define char-set-union
  (case-lambda
    (() char-set:empty)
    ((cs) cs)
    ((cs1 cs2) (char-set-union2 cs1 cs2))
    ((cs1 cs2 . cs*)
     (fold-left char-set-union (char-set-union cs1 cs2) cs*))))

(define (char-set-intersection2 cs1 cs2)
  (define N 2)
  (call-with-string-output-port
    (lambda (p)
      (let ((len1 (string-length cs1))
            (len2 (string-length cs2)))
        (let lp ((i1 0) (i2 0) (count 0))
          (cond ((and (fx<? i1 len1) (fx<? i2 len2))
                 (let ((c1 (string-ref cs1 i1))
                       (c2 (string-ref cs2 i2)))
                   (let-values (((i c i1 i2)
                                 (if (or (char<? c1 c2)
                                         (and (char=? c1 c2)
                                              (fxodd? i1)))
                                     (values i1 c1 (fx+ i1 1) i2)
                                     (values i2 c2 i1 (fx+ i2 1)))))
                     (cond ((fxeven? i)
                            (let ((count (fx+ count 1)))
                              (when (fx=? count N)
                                (put-char p c))
                              (lp i1 i2 count)))
                           (else
                            (when (fx=? count N)
                              (put-char p c))
                            (lp i1 i2 (fx- count 1)))))))
                (else
                 (let ((count (if (or (and (fx<? i1 len1) (fxeven? i1))
                                      (and (fx<? i2 len2) (fxeven? i2)))
                                  (fx+ count 1)
                                  count)))
                   (when (fx=? count N)
                     (put-string p cs1 i1 (fx- len1 i1))
                     (put-string p cs2 i2 (fx- len2 i2)))))))))))

(define char-set-intersection
  (case-lambda
    (() char-set:full)
    ((cs) cs)
    ((cs1 cs2) (char-set-intersection2 cs1 cs2))
    ((cs1 cs2 . cs*)
     (fold-left char-set-intersection (char-set-intersection cs1 cs2) cs*))))

(define char-set-xor
  (case-lambda
    (() char-set:empty)
    ((cs) cs)
    ((cs1 cs2)
     (char-set-union (char-set-intersection (char-set-complement cs1) cs2)
                     (char-set-intersection (char-set-complement cs2) cs1)))
    ((cs1 cs2 . cs*)
     (fold-left char-set-xor (char-set-xor cs1 cs2) cs*))))

(define char-set-difference
  (case-lambda
    ((cs) cs)
    ((cs1 cs2)
     (char-set-intersection cs1 (char-set-complement cs2)))
    ((cs1 cs2 . cs*)
     (fold-left char-set-difference (char-set-difference cs1 cs2) cs*))))

(define (char-set-diff+intersection cs1 . cs*)
  (values (apply char-set-difference cs1 cs*)
          (char-set-intersection cs1 (apply char-set-union cs*))))

(define (char-set . c*)
  (let lp ((cs "") (c* c*))
    (if (null? c*)
        cs
        (lp (char-set-union cs (string (car c*)
                                       (integer->char
                                        (fx+ 1 (char->integer
                                                (car c*))))))
            (cdr c*)))))

(define (char-set->list cs)
  (let lp ((i 0) (ret '()))
    (if (fx=? i (string-length cs))
        ret
        (let ((start (char->integer (string-ref cs i)))
              (end (if (fx=? (fx+ i 1) (string-length cs))
                       (fx+ #x10FFFF 1) ;this seems silly
                       (char->integer (string-ref cs (fx+ i 1))))))
          (let lp* ((c start) (ret ret))
            (cond ((fx=? c end)
                   (if (fx=? (fx+ i 1) (string-length cs))
                       ret
                       (lp (fx+ i 2) ret)))
                  ((fx<=? #xD800 c #xDFFF)
                   (lp* #xE000 ret))
                  (else
                   (lp* (fx+ c 1)
                        (cons (integer->char c) ret)))))))))

(define ucs-range->char-set
  (case-lambda
    ((lower upper)
     (assert (fx<=? lower upper))
     (if (fx=? lower upper)
         ""
         (string (integer->char lower)
                 (integer->char upper))))
    ((lower upper error? base-cs)
     (char-set-union base-cs (ucs-range->char-set lower upper)))))

;; SRFI-14 sets defined here
(define char-set:empty (char-set))
(define char-set:iso-control
  (char-set-union (ucs-range->char-set #x00 #x1F)
                  (ucs-range->char-set #x7F #x9F)))

;; Filled in from tables
(define char-set:lower-case (char-set))
(define char-set:upper-case (char-set))
(define char-set:title-case (char-set)) ;done
(define char-set:letter (char-set))     ;done
(define char-set:symbol (char-set))     ;done
(define char-set:hex-digit (char-set))  ;done
(define char-set:punctuation (char-set)) ;done
(define char-set:whitespace (char-set)) ;done
(define char-set:digit (char-set))

;; char-set:lower-case 	Lower-case letters
;; char-set:upper-case 	Upper-case letters
;; char-set:title-case 	Title-case letters
;; char-set:letter 	Letters
;; char-set:digit 	Digits
;; char-set:letter+digit 	Letters and digits
;; char-set:graphic 	Printing characters except spaces
;; char-set:printing 	Printing characters including spaces
;; char-set:whitespace 	Whitespace characters
;; char-set:iso-control 	The ISO control characters
;; char-set:punctuation 	Punctuation characters
;; char-set:hex-digit 	A hexadecimal digit: 0-9, A-F, a-f
;; char-set:blank 	Blank characters -- horizontal whitespace
;; char-set:ascii 	All characters in the ASCII set.

;; SRFI-14 sets derived from previous sets
(define char-set:full (char-set-complement char-set:empty))
(define char-set:letter+digit (char-set-union char-set:letter
                                              char-set:digit))
(define char-set:graphic (char-set-union char-set:letter
                                         char-set:digit
                                         char-set:punctuation
                                         char-set:symbol)) ;XXX: lots missing!

(define char-set:printing (char-set-union char-set:whitespace
                                          char-set:graphic))

;; For R6RS
(define char-set:r6rs-alphabetic (char-set)) ;R6RS
(define char-set:r6rs-lower-case (char-set)) ;R6RS
(define char-set:r6rs-upper-case (char-set)) ;R6RS
(define char-set:r6rs-whitespace (char-set)) ;R6RS
(define char-set:r6rs-numeric (char-set)) ;R6RS

(define (print-char-set name cs)
  (display "(define ")
  (display name)
  (display " \"")
  (do ((i 0 (fx+ i 1)))
      ((fx=? i (string-length cs))
       (display "\")\n"))
    (display "\\x")
    (display (number->string (char->integer (string-ref cs i))
                             16))
    (display #\;)
    (when (fx=? (fxmod i 10) 9)
      (display "\\\n")))
  (newline))

(define-syntax add-range!
  (syntax-rules ()
    ((_ set start end)
     (set! set (char-set-union set (ucs-range->char-set start end))))))

;; Table of general category for every code point
(define gc-table
  (make-vector (expt 2 21) 'Cn))

;; A list of the general categories (Cc, Zs, etc)
(define gc-names
  (let ((gc-names (make-eq-hashtable)))
    (hashtable-set! gc-names 'Cn #t)
    gc-names))

;; Width for every code point
(define width-table
  (make-vector (expt 2 21) 0))

;; All possible widths (names is a misnomer)
(define width-names
  (let ((width-names (make-eqv-hashtable)))
    (hashtable-set! width-names 0 #t)
    width-names))

(define char-upcase-table (make-vector (expt 2 21) 0)) ; 0 = identity
(define char-downcase-table (make-vector (expt 2 21) 0))
(define char-titlecase-table (make-vector (expt 2 21) 0))

(define (add-general-category cp gc)
  ;; This changes the general category of surrogates to be Co instead,
  ;; because there are huge areas that are Co, so it's gonna compress
  ;; a little better. There's no way to ask char-general-category what
  ;; the category of a surrogate is anyway, because R6RS forbids
  ;; surrogates from becoming characters.
  (let ((gc (if (eq? gc 'Cs)
                'Co
                gc)))
    (hashtable-set! gc-names gc #t)
    (vector-set! gc-table cp gc)))

(define (set-width cp width)
  (hashtable-set! width-names width #t)
  (vector-set! width-table cp width))

(define (process-unicode-data p)
  ;; This isn't good style. Don't do this!
  (define (default x v)
      (if (string=? x "") v x))
    (print ";; Reading unicode data and computing character sets...")
    (vector-fill! char-upcase-table 0)
    (vector-fill! char-downcase-table 0)
    (vector-fill! char-titlecase-table 0)
    (let lp ()
      (let ((line (get-line p)))
        (unless (eof-object? line)
          (match (string-split line #\;)
            ((Character
              Name
              General_Category
              Canonical_Combining_Class
              Bidi_Class
              Decomposition_Type
              Decomposition_Mapping
              Numeric_Type
              Numeric_Value
              Bidi_Mirrored
              Unicode_1_Name
              ISO_Comment
              Simple_Uppercase_Mapping
              Simple_Lowercase_Mapping
              Simple_Titlecase_Mapping)
             (let ((GC (string->symbol General_Category))
                   (UCM (default Simple_Uppercase_Mapping #f))
                   (LCM (default Simple_Lowercase_Mapping #f)))
               (define (handle start end)
                 (do ((i start (fx+ i 1)))
                     ((fx=? i end))
                   (add-general-category i GC))
                 ;;; SRFI-14 char-sets first.
                 ;; char-set:lower-case 	Lower-case letters
                 (when (and (not (fx<=? #x2000 start #x2FFF))
                            (not (fx<=? #x2000 end #x2FFF))
                            (not LCM)
                            (or UCM
                                (string-contains Name "SMALL LETTER")
                                (string-contains Name "SMALL LIGATURE")))
                   (add-range! char-set:lower-case start end))
                 ;; char-set:upper-case 	Upper-case letters
                 (when (and (not (fx<=? #x2000 start #x2FFF))
                            (not (fx<=? #x2000 end #x2FFF))
                            (not UCM)
                            (or LCM
                                (string-contains Name "CAPITAL LETTER")
                                (string-contains Name "CAPITAL LIGATURE")))
                   (add-range! char-set:upper-case start end))
                 ;; char-set:title-case 	Title-case letters
                 (case GC
                   ((Lt)
                    ;; Same in R6RS
                    (add-range! char-set:title-case start end)))
                 ;; char-set:letter 	Letters
                 (case GC
                   ((Lu Ll Lt Lm Lo)
                    (add-range! char-set:letter start end)))
                 ;; char-set:digit 	Digits
                 (case GC
                   ((Nd)
                    (add-range! char-set:digit start end)))
                 ;; char-set:whitespace 	Whitespace characters
                 (case GC
                   ((Zs Zl Zp)
                    (add-range! char-set:whitespace start end)))
                 ;; char-set:punctuation 	Punctuation characters
                 (case GC
                   ((Pc Pd Ps Pe Pi Pf Po)
                    (add-range! char-set:punctuation start end)))
                 ;; char-set:symbol 	Symbol characters
                 (case GC
                   ((Sm Sc Sk So)
                    (add-range! char-set:symbol start end)))
                 ;; char-set:hex-digit 	A hexadecimal digit: 0-9, A-F, a-f
                 ;; char-set:blank 	Blank characters -- horizontal whitespace
                 ;; char-set:ascii 	All characters in the ASCII set.
                 ;;; R6RS char sets
                 (case GC
                   ((Lu Ll Lt Lm Lo Nl)
                    ;; FIXME: Other_Alphabetic
                    (add-range! char-set:r6rs-alphabetic start end)))
                 (case GC
                   ((Lu)
                    (add-range! char-set:r6rs-upper-case start end)))
                 (case GC
                   ((Ll)
                    (add-range! char-set:r6rs-lower-case start end))))
               (cond ((string-suffix? ", First>" Name)
                      ;; Range of characters
                      (match (string-split (get-line p) #\;)
                        ((EndCharacter EndName . x)
                         (let ((start (string->number Character 16))
                               (end (string->number EndCharacter 16)))
                           (handle start (fx+ end 1))))))
                     (else
                      ;; Single character
                      (let ((c (string->number Character 16)))
                        (handle c (+ c 1))
                        ;; Case mapping
                        (let ((UCM (string->number (default Simple_Uppercase_Mapping Character)
                                                   16))
                              (LCM (string->number (default Simple_Lowercase_Mapping Character)
                                                   16))
                              (TCM (string->number (default Simple_Titlecase_Mapping Character)
                                                   16)))
                          (when (> c (char->integer #\delete))
                            (vector-set! char-upcase-table c (- c UCM))
                            (vector-set! char-downcase-table c (- c LCM)))
                          (vector-set! char-titlecase-table c (- c TCM)))))))
             (lp)))))))

(define (remove-comments line)
  (match (string-split line #\#)
    ((data . _)
     (string-filter (srfi14:char-set-complement srfi14:char-set:whitespace)
                    data))
    ((data) data)))

(define (split-range range)
  (match (string-split range #\.)
    ((start "" end)
     (values (string->number start 16)
             (fx+ (string->number end 16) 1)))
    ((start)
     (let ((start (string->number start 16)))
       (values start (fx+ start 1))))))

(define (read-property-file fn)
  (call-with-input-file fn
    (lambda (p)
      (let lp ()
        (let ((line (get-line p)))
          (unless (eof-object? line)
            (match (string-split (remove-comments line) #\;)
              ((Range Property)
               (let-values (((start end) (split-range Range)))
                 (case (string->symbol Property)
                   ((White_Space)
                    (add-range! char-set:whitespace start end)
                    (add-range! char-set:r6rs-whitespace start end))
                   ((Other_Alphabetic)
                    (add-range! char-set:r6rs-alphabetic start end))
                   ((Other_Uppercase)
                    (add-range! char-set:r6rs-upper-case start end))
                   ((Other_Lowercase)
                    (add-range! char-set:r6rs-lower-case start end))
                   ((Numeric Digit Decimal)
                    (add-range! char-set:r6rs-numeric start end))
                   ((ASCII_Hex_Digit)
                    (add-range! char-set:hex-digit start end)))))
              (else #f))
            (lp)))))))

(define (read-east-asian-width-property-file fn)
  (define (set-range start end value)
    (let ((w (case value
               ((W F) 2)
               ((Na H) 1)
               (else #f))))
      (when w
        (do ((i start (fx+ i 1))) ((fx>? i end))
          (set-width i w)))))
  ;; From the comments in EastAsianWidth.txt
  (for-each (lambda (range)
              (let-values (((start end) (split-range range)))
                (set-range start end 'W)))
            '("3400..4DBF" "4E00..9FFF" "F900..FAFF"
              "20000..2FFFD" "30000..3FFFD"))
  (call-with-input-file fn
    (lambda (p)
      (let lp ()
        (let ((line (get-line p)))
          (unless (eof-object? line)
            (match (string-split (remove-comments line) #\;)
              ((Range Value)
               (let-values (((start end) (split-range Range)))
                 (set-range start end (string->symbol Value))))
              (else #f))
            (lp)))))))

(define (**print-char-sets**)
  (print-char-set 'char-set:r6rs-alphabetic char-set:r6rs-alphabetic)
  (print-char-set 'char-set:r6rs-lower-case char-set:r6rs-lower-case)
  (print-char-set 'char-set:r6rs-upper-case char-set:r6rs-upper-case)
  (print-char-set 'char-set:r6rs-whitespace char-set:r6rs-whitespace)
  (print-char-set 'char-set:r6rs-numeric char-set:r6rs-numeric)
  (print-char-set 'char-set:r6rs-title-case char-set:title-case))

(define (**print-predicates**)
  ;; The logic here is questionable.
  (pretty-print
   '(begin
      ;; '''These procedures return #t if their arguments are alpha-
      ;; betic, numeric, whitespace, upper-case, lower-case, or title-
      ;; case characters, respectively; otherwise they return #f.
      ;; A character is alphabetic if it has the Unicode "Alpha-
      ;; betic" property. A character is numeric if it has the Uni-
      ;; code "Numeric" property. A character is whitespace if has
      ;; the Unicode "White Space" property. A character is upper
      ;; case if it has the Unicode "Uppercase" property, lower case
      ;; if it has the "Lowercase" property, and title case if it is in
      ;; the Lt general category.'''
      (define (char-alphabetic? char)
        ;; Defined as Lu + Ll + Lt + Lm + Lo + Nl + Other_Alphabetic
        (char-set-contains? char-set:r6rs-alphabetic char))
      (define (char-numeric? char)
        ;; Not defined properly by Unicode?
        (char-set-contains? char-set:r6rs-numeric char))
      (define (char-whitespace? char)
        ;; "White Space" property
        (char-set-contains? char-set:r6rs-whitespace char))
      (define (char-upper-case? char)
        ;; Defined as Lu + Other_Uppercase
        (char-set-contains? char-set:r6rs-upper-case char))
      (define (char-lower-case? char)
        ;; Defined as Ll + Other_Lowercase
        (char-set-contains? char-set:r6rs-lower-case char))
      (define (char-title-case? char)
        ;; Is char in the Lt general category?
        (char-set-contains? char-set:title-case char))
      (define (char-set-contains? cs c)
        (let ((ci (char->integer c)))
          (let lp ((low 0)
                   (high (string-length cs)))
            (if (fx<? low high)
                (let ((middle (fxarithmetic-shift-right (fx+ low high) 1)))
                  (if (fx>=? ci (char->integer (string-ref cs middle)))
                      (lp (fx+ middle 1) high)
                      (lp low middle)))
                (fxodd? high)))))))
  (newline))

;;; Load data files

(call-with-input-file "data/UnicodeData.txt"
  process-unicode-data)

(do ((i 0 (fx+ i 1)))
    ((fx=? i (vector-length gc-table)))
  ;; Reasonable defaults for character width
  (if (memq (vector-ref gc-table i) '(Mn Mc Me Sk Zs Zl Zp Cc Cf Cs))
      (set-width i 0)
      (set-width i 1)))

(read-property-file "data/PropList.txt")

(read-property-file "data/extracted/DerivedNumericType.txt")

(read-east-asian-width-property-file "data/EastAsianWidth.txt")

;;; Compress gc-table into a two-level lookup table

;; This is where it gets strange.
(print ";; Compressing general category table...")

;; First assign an index to each of the general categories.
(define gc-name-vector
  (let ((v (hashtable-keys gc-names)))
    (vector-sort! (lambda (x y)
                    (string<? (symbol->string x)
                              (symbol->string y)))
                  v)
    (do ((i 0 (+ i 1)))
        ((= i (vector-length v)))
      (hashtable-set! gc-names (vector-ref v i) i))
    (print ";; " v)
    (print ";; There are " (hashtable-size gc-names) " categories")
    v))

(define (vector-shuffle! v)
  ;; You know what. Don't use this. I have no idea if it's a perfect
  ;; shuffle or what.
  (do ((n (vector-length v) (- n 1)))
      ((eqv? n 0))
    (vector-swap! v (random-integer n) (- n 1))))

(define (compress =? table block-size)
  (define data (make-vector (vector-length table) #f)) ;will shrink later
  (define n 0)                          ;elements in data
  (define (fits? table-i data-i)
    (let lp ((i 0))
      (or (= i block-size)
          (and (or (not (vector-ref data (+ data-i i)))
                   (=? (vector-ref data (+ data-i i))
                       (vector-ref table (+ table-i i))))
               (lp (+ i 1))))))
  (define (find-index table-i)
    ;; Search 'data' vector for a place where the entries starting at
    ;; index 'i' of 'table' match (or the elements in 'data' are #f).
    ;; Then copy those entries from 'table' to 'data'.
    (do ((j 0 (+ j 1)))
        ((fits? table-i j)
         (set! n (max n (+ j block-size)))
         (vector-copy! data j table table-i (+ table-i block-size))
         j)))
  (define (block-index-table)
    (do ((i 0 (+ i 1))
         (v (make-vector (/ (vector-length table)
                            block-size))))
        ((= i (vector-length v))
         v)
      (vector-set! v i (* i block-size))))
  (let ((indexes (make-vector (/ (vector-length table)
                                 block-size)
                              #f)))
    ;; The table must be a multiple of the block-size.
    (let ((block-indexes (block-index-table)))
      (vector-shuffle! block-indexes)
      ;; (display block-indexes) (newline)
      (do ((i 0 (+ i 1)))
          ((= i (vector-length indexes))
           (values indexes
                   (vector-copy data 0 n)))
        (let* ((table-i (vector-ref block-indexes i))
               (index-i (/ table-i block-size)))
          ;; (display (list table-i index-i)) (newline)
          (vector-set! indexes index-i (find-index table-i)))))))

(define (check-table =? original-table index data width)
  (define fxasr fxarithmetic-shift-right)
  ;; Check that the compressed table is correct.
  (let ((shift width)
        (mask (- (expt 2 width) 1)))
    (do ((i 0 (+ i 1)))
        ((= i (vector-length original-table)))
      (let ((expect (vector-ref original-table i))
            (have (vector-ref data (+ (vector-ref index (fxasr i shift))
                                      (fxand i mask)))))
        (unless (=? expect have)
          (error 'check-table "Bunko!" i expect have))))))

(define (compress2 =? table data-width index-width)
  ;;(print "Optimizing first level:")
  (let-values (((index0 data) (compress eq? table (expt 2 data-width))))
    ;;(print "Checking first level.")
    ;; (check-table eq? table index0 data data-width)
    ;;(print "Optimizing second level:")
    (let-values (((index1 index2) (compress eqv? index0 (expt 2 index-width))))
      ;;(print "Checking second level.")
      ;; (check-table eqv? index0 index1 index2 index-width)
      ;; Now 'index0' is no longer needed, except do to the check.
      (values index0 index1 index2 data))))

(define (data-size index1 index2 data)
  ;; Ignore static overhead, assume the data vector is encoded as a
  ;; bytevector. index1 and index2 probably need two bytes per entry
  ;; (but it's actually different for different printed procedures),
  ;; but just count each entry as one Q. index0 is not needed at
  ;; runtime, because it is contained in index1 and index2.
  (+ (div (vector-length data) Q)
     (vector-length index1)
     (vector-length index2)))

(define (minimize-compress2 =? table data-width index-width permutations)
  (display ";; " (current-error-port))
  (let lp ((i 0)
           (index0 #f)
           (index1 #f)
           (index2 #f)
           (data #f)
           (best +inf.0)
           (worst -inf.0))
    (cond ((= i permutations)
           (print "\n;; Worst size: " worst " best size: " best " "
                  (list (inexact (/ best worst))))
           (values index0 index1 index2 data))
          (else
           (let-values (((index0^ index1^ index2^ data^)
                         (compress2 =? table data-width index-width)))
             (let ((size (data-size index1^ index2^ data^)))
               (let ((worst (exact (max worst size))))
                 (cond ((< size best)
                        (display size (current-error-port)) (display ".. " (current-error-port))
                        (flush-output-port (current-error-port))
                        (lp (+ i 1) index0^ index1^ index2^ data^ size worst))
                       (else
                        (lp (+ i 1) index0 index1 index2 data best worst))))))))))

(define (print-cgc-program procname index1 index2 data data-width index-width
                           names name-vector)
  (define (u16le-vector->bytevector x endianness)
    (uint-list->bytevector (vector->list x) endianness 2))
  (pretty-print
   `(define (,procname cp)
      (define cat-index1 ',(u16le-vector->bytevector index1 (endianness little)))
      (define cat-index2 ',(u16le-vector->bytevector index2 (endianness little)))
      (define cat-data
        ,(u8-list->bytevector
          (map (lambda (category)
                 (hashtable-ref names category #f))
               (vector->list data))))
      (define values ',name-vector)
      (define data-width ,data-width)
      (define index-width ,index-width)
      (define (bf n start end)
        (let ((mask (fxnot (fxarithmetic-shift-left -1 end))))
          (fxarithmetic-shift-right (fxand n mask) start)))
      (define bytevector-u16-le-ref
        (if (eq? (native-endianness) 'little)
            bytevector-u16-native-ref
            (lambda (bv idx) (bytevector-u16-ref bv idx (endianness little)))))
      (let* ((i (char->integer cp))
             (l2
              (bytevector-u16-le-ref
               cat-index1 (fx* 2 (bf i (+ data-width index-width) 22))))
             (l3
              (bytevector-u16-le-ref
               cat-index2 (fx* 2 (fx+ l2 (bf i data-width (+ data-width index-width))))))
             (idx
              (bytevector-u8-ref
               cat-data (fx+ l3 (bf i 0 data-width)))))
        (vector-ref values idx))))
  (newline))

(define (**print-general-category**)
  (let ((data-width 7)
        (index-width 5))
    (let-values (((index0 index1 index2 data)
                  (minimize-compress2 eq? gc-table
                                      data-width index-width
                                      effort:general-category)))
      (check-table eq? gc-table index0 data data-width)
      (check-table eqv? index0 index1 index2 index-width)
      (let ((sizec (data-size index1 index2 data))
            (sizeu (div (vector-length gc-table) 8)))
        (print ";; Table size: " sizec" Q")
        (print ";; Uncompressed: " sizeu " Q")
        (print ";; Ratio: " (inexact (/ sizec sizeu))))
      (print-cgc-program 'char-general-category
                         index1 index2 data data-width index-width
                         gc-names gc-name-vector))))

(define (**search-best-table-widths** procname table names names-vector)
  (let lp ((d/i* (append-map (lambda (x)
                               (map (lambda (y) (cons x y))
                                    (iota 9 2)))
                             (iota 9 2)))
           (index1 #f)
           (index2 #f)
           (data #f)
           (data-width #f)
           (index-width #f)
           (size +inf.0))
    (cond ((null? d/i*)
           (let ((sizeu (div (vector-length width-table) Q)))
             (print ";; Table size: " size" Q")
             (print ";; Uncompressed: " sizeu " Q")
             (print ";; Ratio: " (inexact (/ size sizeu))))
           (print-cgc-program procname
                              index1 index2 data data-width index-width
                              names names-vector))
          (else
           (let ((data-width^ (caar d/i*))
                 (index-width^ (cdar d/i*)))
             (print "\n;; data-width: " data-width^ " index-width: " index-width^
                    " (best so far: " data-width "," index-width "," size ")")
             (let-values (((index0^ index1^ index2^ data^)
                           (minimize-compress2 eq? table
                                               data-width^ index-width^
                                               effort:general-category)))
               (check-table eq? table index0^ data^ data-width^)
               (check-table eqv? index0^ index1^ index2^ index-width^)
               (let ((size^ (data-size index1^ index2^ data^)))
                 (if (< size^ size)
                     (lp (cdr d/i*)
                         index1^ index2^ data^
                         data-width^ index-width^ size^)
                     (lp (cdr d/i*)
                         index1 index2 data
                         data-width index-width size)))))))))

;;; Character width

(define width-names-vector
  (let ((v (hashtable-keys width-names)))
    (vector-sort! < v)
    (do ((i 0 (+ i 1)))
        ((= i (vector-length v)))
      (hashtable-set! width-names (vector-ref v i) i))
    (print ";; " v)
    (print ";; There are " (hashtable-size width-names) " possible widths")
    v))

(define (**print-char-width**)
  (let ((data-width 6)
        (index-width 6))
    (let-values (((index0 index1 index2 data)
                  (minimize-compress2 eq? width-table
                                      data-width index-width
                                      effort:general-category)))
      (check-table eqv? width-table index0 data data-width)
      (check-table eqv? index0 index1 index2 index-width)
      (let ((sizec (data-size index1 index2 data))
            (sizeu (div (vector-length gc-table) Q)))
        (print ";; Table size: " sizec" Q")
        (print ";; Uncompressed: " sizeu " Q")
        (print ";; Ratio: " (inexact (/ sizec sizeu))))
      (print-cgc-program 'char-width
                         index1 index2 data data-width index-width
                         width-names width-names-vector))))

;; (**search-best-table-widths** 'char-general-category gc-table gc-names gc-name-vector)

;; (**search-best-table-widths** 'char-width width-table width-names width-names-vector)

;;; Case conversion

(print ";; Case conversion...")

(define (search-best-widths table permutations)
  (let lp ((d/i* (append-map (lambda (x)
                               (map (lambda (y) (cons x y))
                                    (iota 9 2)))
                             (iota 9 2)))
           (index1 #f)
           (index2 #f)
           (data #f)
           (data-width #f)
           (index-width #f)
           (size +inf.0))
    (cond ((null? d/i*)
           (let ((sizeu (div (vector-length table) 8)))
             (print ";; Table size: " size" Q")
             (print ";; Uncompressed: " sizeu " Q")
             (print ";; Ratio: " (inexact (/ size sizeu))))
           (values index1 index2 data data-width index-width))
          (else
           (let ((data-width^ (caar d/i*))
                 (index-width^ (cdar d/i*)))
             (print "\ndata-width: " data-width^ " index-width: " index-width^
                    " (best so far: " data-width "," index-width "," size ")")
             (let-values (((index0^ index1^ index2^ data^)
                           (minimize-compress2 eq? table
                                               data-width^ index-width^
                                               effort:best-widths)))
               (check-table eq? table index0^ data^ data-width^)
               (check-table eqv? index0^ index1^ index2^ index-width^)
               (let ((size^ (data-size index1^ index2^ data^)))
                 (if (< size^ size)
                     (lp (cdr d/i*)
                         index1^ index2^ data^
                         data-width^ index-width^ size^)
                     (lp (cdr d/i*)
                         index1 index2 data
                         data-width index-width size)))))))))

;; (search-best-widths char-upcase-table 4)

(define (vector-for-each* proc v)
  (do ((i 0 (+ i 1)))
      ((= i (vector-length v)))
    (proc (vector-ref v i) i)))

;; Into the North Window of my chamber glows the Pole Star with uncanny light.
(define (print-upcase-program index1 index2 data data-width index-width)
  (define (u16le-vector->bytevector x endianness)
    (uint-list->bytevector (vector->list x) endianness 2))
  (define offsets (make-eqv-hashtable))
  (define offset-vector
    (begin
      (vector-for-each* (lambda (offset i)
                          (hashtable-set! offsets offset #t))
                        data)
      (let ((x (hashtable-keys offsets)))
        (vector-sort! < x)
        (vector-for-each* (lambda (offset i)
                            (hashtable-set! offsets offset i))
                          x)
        x)))
  (pretty-print
   `(define (char-upcase c)
      (define index1 ',index1)
      (define index2 ',index2)
      (define data ',(u8-list->bytevector
                      (map (lambda (offset)
                             (hashtable-ref offsets offset #f))
                           (vector->list data))))
      (define offsets ',offset-vector)
      (define data-width ,data-width)
      (define index-width ,index-width)
      (define fxasr fxarithmetic-shift-right)
      (define fxasl fxarithmetic-shift-left)
      (define (bf n start end)
        (let ((mask (fxnot (fxasl -1 end))))
          (fxasr (fxand n mask) start)))
      (assert (eq? (native-endianness) 'little))
      (let ((i (char->integer c)))
        (if (fx<=? (char->integer #\a) i (char->integer #\z))
            (integer->char (fxand i (fxnot (fxxor (char->integer #\A)
                                                  (char->integer #\a)))))
            (let* ((l2
                    (vector-ref
                     index1 (bf i (+ data-width index-width) 22)))
                   (l3
                    (vector-ref
                     index2 (fx+ l2 (bf i data-width (+ data-width index-width)))))
                   (idx
                    (bytevector-u8-ref
                     data (fx+ l3 (bf i 0 data-width)))))
              (integer->char (fx- i (vector-ref offsets idx))))))))
  (newline))

;; All through the long hellish hours of blackness it shines there.
(define (print-downcase-program index1 index2 data data-width index-width)
  (define (u16le-vector->bytevector x endianness)
    (uint-list->bytevector (vector->list x) endianness 2))
  (define offsets (make-eqv-hashtable))
  (define offset-vector
    (begin
      (vector-for-each* (lambda (offset i)
                          (hashtable-set! offsets offset #t))
                        data)
      (let ((x (hashtable-keys offsets)))
        (vector-sort! < x)
        (vector-for-each* (lambda (offset i)
                            (hashtable-set! offsets offset i))
                          x)
        x)))
  (pretty-print
   `(define (char-downcase c)
      (define index1 ',index1)
      (define index2 ',index2)
      (define data ',(u8-list->bytevector
                      (map (lambda (offset)
                             (hashtable-ref offsets offset #f))
                           (vector->list data))))
      (define offsets ',offset-vector)
      (define data-width ,data-width)
      (define index-width ,index-width)
      (define fxasr fxarithmetic-shift-right)
      (define fxasl fxarithmetic-shift-left)
      (define (bf n start end)
        (let ((mask (fxnot (fxasl -1 end))))
          (fxasr (fxand n mask) start)))
      (let ((i (char->integer c)))
        (if (fx<=? (char->integer #\A) i (char->integer #\Z))
            (integer->char (fxior i (fxxor (char->integer #\A)
                                           (char->integer #\a))))
            (let* ((l2
                    (vector-ref
                     index1 (bf i (+ data-width index-width) 22)))
                   (l3
                    (vector-ref
                     index2 (fx+ l2 (bf i data-width (+ data-width index-width)))))
                   (idx
                    (bytevector-u8-ref
                     data (fx+ l3 (bf i 0 data-width)))))
              (integer->char (fx- i (vector-ref offsets idx))))))))
  (newline))

;; And it was under a horned waning moon that I saw the city for the first time.
(define (print-titlecase-program index1 index2 data data-width index-width)
  (define (u16le-vector->bytevector x endianness)
    (uint-list->bytevector (vector->list x) endianness 2))
  (define offsets (make-eqv-hashtable))
  (define offset-vector
    (begin
      (vector-for-each* (lambda (offset i)
                          (hashtable-set! offsets offset #t))
                        data)
      (let ((x (hashtable-keys offsets)))
        (vector-sort! < x)
        (vector-for-each* (lambda (offset i)
                            (hashtable-set! offsets offset i))
                          x)
        x)))
  (pretty-print
   `(define (char-titlecase c)
      (define index1 ',index1)
      (define index2 ',index2)
      (define data ',(u8-list->bytevector
                      (map (lambda (offset)
                             (hashtable-ref offsets offset #f))
                           (vector->list data))))
      (define offsets ',offset-vector)
      (define data-width ,data-width)
      (define index-width ,index-width)
      (define fxasr fxarithmetic-shift-right)
      (define fxasl fxarithmetic-shift-left)
      (define (bf n start end)
        (let ((mask (fxnot (fxasl -1 end))))
          (fxasr (fxand n mask) start)))
      (let ((i (char->integer c)))
        (let* ((l2
                (vector-ref
                 index1 (bf i (+ data-width index-width) 22)))
               (l3
                (vector-ref
                 index2 (fx+ l2 (bf i data-width (+ data-width index-width)))))
               (idx
                (bytevector-u8-ref
                 data (fx+ l3 (bf i 0 data-width)))))
          (integer->char (fx- i (vector-ref offsets idx)))))))
  (newline))

;; Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn.
(define (**print-case-programs**)
  (let ((data-width 8)
        (index-width 6))
    (let-values (((_ index1 index2 data)
                  (minimize-compress2 eqv? char-upcase-table
                                      data-width index-width
                                      effort:case-programs)))
      (print-upcase-program index1 index2 data data-width index-width))
    (let-values (((_ index1 index2 data)
                  (minimize-compress2 eqv? char-downcase-table
                                      data-width index-width
                                      effort:case-programs)))
      (print-downcase-program index1 index2 data data-width index-width))
    (let-values (((_ index1 index2 data)
                  (minimize-compress2 eqv? char-titlecase-table
                                      data-width index-width
                                      effort:case-programs)))
      (print-titlecase-program index1 index2 data data-width index-width))))


(when (file-exists? "unidata.scm")
  (delete-file "unidata.scm"))
(with-output-to-file "unidata.scm"
  (lambda ()
    (**print-copyright**)
    (**print-char-sets**)
    (**print-predicates**)
    (**print-general-category**)
    (**print-case-programs**)
    (**print-char-width**)))
(display "Done writing unidata.scm\n")
